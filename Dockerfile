FROM python:3.7

WORKDIR /src
COPY . /src

# Install packages
RUN pip install --no-cache-dir -r requirements.txt

# Expose a port for runserver
EXPOSE 8001

# run celery worker and runserver
ENTRYPOINT  celery multi start worker -A app.celery --loglevel=info -f celery_logs.txt && python manage.py runserver -h 0.0.0.0 -p 8001