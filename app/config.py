import os
from app.constants import REPORTS_DEFAULT_LOCATION, PRODUCTION_ENV


class Config:
    DEBUG = False
    TESTING = False
    REDIS_HOST = os.environ.get("REDIS_HOST", "localhost")
    REDIS_PORT = os.environ.get("REDIS_PORT", 6379)
    REDIS_DB = os.environ.get("REDIS_DB", 0)
    REPORTS_PATH = os.environ.get('REPORTS_PATH', REPORTS_DEFAULT_LOCATION)


class DevConfig(Config):
    DEBUG = True


class ProdConfig(Config):
    pass


def config_handler():
    if os.environ.get('APP_ENV') == PRODUCTION_ENV:
        return ProdConfig()
    return DevConfig()
