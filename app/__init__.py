from flask import Flask

from app.api import register_api
from app.config import config_handler
from app.services.celery import make_celery

app = Flask(__name__)
app.config.from_object(config_handler())

celery = make_celery(app)

register_api(app)
