REPORT_NAME_PREFIX = 'report-'
REPORTS_DEFAULT_LOCATION = 'reports/'

SERVICE_NAME = "notification-manager"

DEVELOPMENT_ENV = 'dev'
PRODUCTION_ENV = 'prod'
