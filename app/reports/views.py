import os
import time

from flask import request, jsonify
from flask.views import MethodView

from app import app
from app.constants import REPORT_NAME_PREFIX
from app.services.celery.tasks import generate_report


class ReportGeneratorView(MethodView):
    def post(self):
        generate_report.apply_async(args=[REPORT_NAME_PREFIX + str(time.time()), request.json])
        return 'file is going to be created'


class ReportsView(MethodView):
    def get(self):
        items = os.listdir(app.config['REPORTS_PATH'])
        return jsonify([i for i in items if i.startswith(REPORT_NAME_PREFIX)])



