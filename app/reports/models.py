from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime

Base = declarative_base()


class Reports(Base):
    __tablename__ = 'reports'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime)
    job_id = Column(String)
    path = Column(String)
    status = Column(String)

    def __repr__(self):
        return f"<Reports(id={self.id}, " \
           f"created_at={self.created_at}, " \
           f"job_id={self.job_id}, " \
           f"path={self.path}, " \
           f"status={self.status})>"
