import pathlib

from app import app


class ReportGenerator:
    reports_path = app.config['REPORTS_PATH']

    def generate_report(self, filename, data, extension):
        pathlib.Path(self.reports_path).mkdir(parents=True, exist_ok=True)
        file_path = self.reports_path + filename + extension
        with open(file_path, 'w') as f:
            for line in data:
                f.write(str(line) + '\n')
