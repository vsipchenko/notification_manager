from app import celery
from app.reports.utils import ReportGenerator


@celery.task()
def generate_report(filename, data, extension='.txt'):
    ReportGenerator().generate_report(filename, data, extension)
