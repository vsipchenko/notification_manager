from celery import Celery


def make_celery(app):
    redis_url = f'redis://{app.config["REDIS_HOST"]}:{app.config["REDIS_PORT"]}/{app.config["REDIS_DB"]}'
    celery = Celery(app.name, broker=redis_url, backend=redis_url, include=['app.services.celery.tasks'])
    celery.config_from_object(app.config, namespace='CELERY')
    return celery
