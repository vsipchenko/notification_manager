from flask.views import MethodView

from app.constants import SERVICE_NAME
from app.reports import bp as reports_bp


class SmokeView(MethodView):
    def get(self):
        return "PONG!"


def register_api(app):
    from app.reports.views import ReportGeneratorView, ReportsView
    url_prefix = f"/{SERVICE_NAME}"
    # smoke api
    app.add_url_rule(url_prefix + '/smoke', view_func=SmokeView.as_view('SmokeView'), strict_slashes=False)

    # reports apis
    reports_bp.add_url_rule('/', view_func=ReportsView.as_view('ReportsView'), strict_slashes=False)
    reports_bp.add_url_rule('/generate', view_func=ReportGeneratorView.as_view('ReportGeneratorView'),
                            strict_slashes=False)

    app.register_blueprint(reports_bp, url_prefix=url_prefix + '/reports')
