import argparse
import sys

import commands as cmd


def parse_args(args):
    parser = argparse.ArgumentParser(description="Flask skeleton", add_help=False)
    subparsers = parser.add_subparsers(dest="command")

    sparser = subparsers.add_parser(cmd.runserver.__name__, add_help=False, help="Runserver on specific port")
    sparser.add_argument("-h", "--host", dest="host", default="0.0.0.0", type=str, help="Host address")
    sparser.add_argument("-p", "--port", dest="port", default=5000, type=int, help="Host post")

    return parser.parse_args(args=args)


def main(args=None):

    parsed_args = parse_args(args or sys.argv[1:])

    if parsed_args.command == cmd.runserver.__name__:
        cmd.runserver(parsed_args.host, parsed_args.port)

    raise Exception("Invalid command")


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e)
        exit(1)
